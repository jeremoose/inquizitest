# inQuiziTest

inQuiziTest is a web based tool to create exams and quizzes and let students take them. It can also track the progress of the students and create reports including students' grades, progress, etc.
